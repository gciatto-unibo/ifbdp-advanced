#!/bin/bash

# https://kubernetes.io/docs/setup/independent/install-kubeadm/
USERNAME=ubuntu
USER_HOME=/home/$USERNAME
DOCKER_SWARM_INIT_FILE=$USER_HOME/docker.log.txt
HOSTNAME=`hostname`

function log {
    echo "    [GCis] $1"
}

function execute_on_worker_sudo {
    echo $"ssh -oStrictHostKeyChecking=no \"$USERNAME@worker-$1-node.local\" \"sudo\" $2"
    ssh -oStrictHostKeyChecking=no "$USERNAME@worker-$1-node.local" "sudo" $2
}

echo ; echo "################### Giovanni Ciatto's initialisation script ####################"

echo "127.0.0.1		$HOSTNAME" >> /etc/hosts

apt-get -qq update
apt-get -qq  install -y docker.io=17.03.2-0ubuntu2~16.04.1 avahi-daemon avahi-discover avahi-utils libnss-mdns mdns-scan ipvsadm \
    && log "Docker & mDNS correctly installed" \
    || log "Problem while installing Docker or mDNS"

apt-get -qq update && apt-get -qq install -y apt-transport-https curl w3m \
    && log "HTTP utils correctly installed" \
    || log "Problem while installing HTTP utils"

modprobe ip_vs && log "IPVS modules loaded" || log "Problem while loading IPVS module"

service docker start && log "Docker service started" || log "Problem while starting the docker service"

usermod -a -G docker $USERNAME && log "$USERNAME added to the docker group" || log "Problem while adding $USERNAME to the docker group"

docker swarm init > $DOCKER_SWARM_INIT_FILE && log "Docker swarm correctly initialised" || log "Problem while initialising docker swarm"

log "The $DOCKER_SWARM_INIT_FILE file:"
cat $DOCKER_SWARM_INIT_FILE


echo "$PRIVATE_KEY" >> $USER_HOME/.ssh/id_rsa
chown $USERNAME:$USERNAME $USER_HOME/.ssh/id_rsa
chmod 400 $USER_HOME/.ssh/id_rsa
echo "$PRIVATE_KEY" >> ~/.ssh/id_rsa
chmod 400 ~/.ssh/id_rsa

JOIN_CMD=`cat $DOCKER_SWARM_INIT_FILE | grep "    "`

log "JOIN_CMD = \"$JOIN_CMD\""

for i in `seq 1 $N_WORKERS`; do
    execute_on_worker_sudo "$(($i - 1))" "$JOIN_CMD"
done

echo "################ End of Giovanni Ciatto's initialisation script ################" ; echo

