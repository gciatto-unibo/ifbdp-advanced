# count_lines_in_pods "tier=frontend" "GET"
function count_lines_in_pods {
    for pod in $(kubectl get pod -l $1 -o=jsonpath='{range .items[*]}{.metadata.name}{"\n"}'); do
        echo "$pod : $(kubectl logs $pod | grep $2 | wc -l)";
    done
}

SVC_URL=http://192.168.99.100:30977
for i in `seq 1 100`; do 
    curl $SVC_URL; 
    count_lines_in_pods "tier=frontend" "GET"; 
    echo "---"; 
done