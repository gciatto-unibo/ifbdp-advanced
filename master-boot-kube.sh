#!/bin/bash

# https://kubernetes.io/docs/setup/independent/install-kubeadm/
USERNAME=ubuntu
USER_HOME=/home/$USERNAME
KUBEADM_INIT_FILE=$USER_HOME/kubeadm.log.txt
HOSTNAME=`hostname`

ETC_KUBE=/etc/kubernetes
CLOUD_CONF=$ETC_KUBE/cloud.conf
ETC_KUBE_MANIFESTS=$ETC_KUBE/manifests
KUBE_CONTROLLER_MANAGER=$ETC_KUBE_MANIFESTS/kube-controller-manager.yaml
KUBE_API_SERVER=$ETC_KUBE_MANIFESTS/kube-apiserver.yaml

KUBE_ADM_CONF=/etc/systemd/system/kubelet.service.d/10-kubeadm.conf

function log {
    echo "    [GCis] $1"
}

function execute_on_worker_sudo {
    echo $"ssh -oStrictHostKeyChecking=no \"$USERNAME@worker-$1-node.local\" \"sudo\" $2"
    ssh -oStrictHostKeyChecking=no "$USERNAME@worker-$1-node.local" "sudo" $2
}

function scp_root_file_on_worker {
    BASENAME=`basename $1`
    scp -oStrictHostKeyChecking=no "$1" "$USERNAME@worker-$2-node.local:$BASENAME"
    ssh -oStrictHostKeyChecking=no "$USERNAME@worker-$2-node.local" sudo chown root:root $BASENAME
    ssh -oStrictHostKeyChecking=no "$USERNAME@worker-$2-node.local" sudo cp $BASENAME $3
}

function generate_cloud_conf {
    echo '[Global]'                     >  $1
    echo "username=$OS_USERNAME"        >> $1
    echo "password=$OS_PASSWORD"        >> $1
    echo "auth-url=$OS_AUTH_URL"        >> $1
    echo "tenant-id=$OS_TENANT_ID"      >> $1
    echo "domain-name=$OS_DOMAIN_NAME"  >> $1
    echo "region=$OS_REGION"            >> $1
    echo                                >> $1
    echo '[LoadBalancer]'               >> $1
    echo "subnet-id=$SUBNET_ID"         >> $1
    echo "floating-network-id=$FLOATING_NETWORK_ID" >> $1
}

echo ; echo "################### Giovanni Ciatto's initialisation script ####################"

echo "127.0.0.1		$HOSTNAME" >> /etc/hosts

apt-get -qq update
apt-get -qq  install -y docker.io avahi-daemon avahi-discover avahi-utils libnss-mdns mdns-scan \
    && log "Docker & mDNS correctly installed" \
    || log "Problem while installing Docker or mDNS"

apt-get -qq update && apt-get -qq install -y apt-transport-https curl \
    && log "HTTP utils correctly installed" \
    || log "Problem while installing HTTP utils"

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF

KUBE_VERSION=1.11.3-00

apt-get -qq update
apt-get -qq install -y kubelet=$KUBE_VERSION kubeadm=$KUBE_VERSION kubectl=$KUBE_VERSION \
    && log "Kube* correctly installed" \
    || log "Problem while installing Kube*"
apt-mark hold kubelet kubeadm kubectl

kubeadm config images pull \
    && log "Checking docker images ok" \
    || log "Problems with kube* docker images"

kubeadm init --pod-network-cidr=10.244.0.0/16 > $KUBEADM_INIT_FILE
chown $USERNAME:$USERNAME $KUBEADM_INIT_FILE
chmod 444 $KUBEADM_INIT_FILE

mkdir -p $USER_HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $USER_HOME/.kube/config
sudo chown $USERNAME:$USERNAME $USER_HOME/.kube/config
export KUBECONFIG=/etc/kubernetes/admin.conf

sysctl net.bridge.bridge-nf-call-iptables=1

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml \
    && echo "Flannel correctly applied" \
    || echo "Problem while applying Flannel"

# kubectl taint nodes --all node-role.kubernetes.io/master-

echo "$PRIVATE_KEY" >> $USER_HOME/.ssh/id_rsa
chown $USERNAME:$USERNAME $USER_HOME/.ssh/id_rsa
chmod 400 $USER_HOME/.ssh/id_rsa
echo "$PRIVATE_KEY" >> ~/.ssh/id_rsa
chmod 400 ~/.ssh/id_rsa

JOIN_CMD=`cat $KUBEADM_INIT_FILE | grep "kubeadm join"`

for i in `seq 1 $N_WORKERS`; do
    execute_on_worker_sudo "$(($i - 1))" "$JOIN_CMD"
done

generate_cloud_conf $CLOUD_CONF

PREFIX1='    -'
PREFIX2='     '
PREFIX3='  -'
PREFIX4='   '
CC=`echo $CLOUD_CONF | sed -e "s/\//\\\\\\\\\//g"`
COMMAND_CLOUD_PROVIDER="$PREFIX1 --cloud-provider=openstack"
COMMAND_CLOUD_CONFIG="$PREFIX1 --cloud-config=$CC"
VALUME_MOUNTS_PATH="$PREFIX1 mountPath: $CC"
VALUME_MOUNTS_NAME="$PREFIX2 name: cloud-config"
VALUME_MOUNTS_RO="$PREFIX2 readOnly: true"
VOLUMES_HOSTPATH="$PREFIX3 hostPath:"
VOLUMES_HOSTPATH_PATH="$PREFIX2 path: $CC"
VOLUMES_HOSTPATH_TYPE="$PREFIX2 type: FileOrCreate"
VOLUMES_NAME="$PREFIX4 name: cloud-config"

sed -e "s/\(- kube-controller-manager\)/\\1\\n$COMMAND_CLOUD_PROVIDER\\n$COMMAND_CLOUD_CONFIG/g" \
    -e "s/\(volumeMounts:\)/\\1\\n$VALUME_MOUNTS_PATH\\n$VALUME_MOUNTS_NAME\\n$VALUME_MOUNTS_RO/g" \
    -e "s/\(volumes:\)/\\1\\n$VOLUMES_HOSTPATH\\n$VOLUMES_HOSTPATH_PATH\\n$VOLUMES_HOSTPATH_TYPE\\n$VOLUMES_NAME/g" \
    -i $KUBE_CONTROLLER_MANAGER

sed -e "s/\(- kube-apiserver\)/\\1\\n$COMMAND_CLOUD_PROVIDER\\n$COMMAND_CLOUD_CONFIG/g" \
    -e "s/\(volumeMounts:\\)/\\1\\n$VALUME_MOUNTS_PATH\n$VALUME_MOUNTS_NAME\\n$VALUME_MOUNTS_RO/g" \
    -e "s/\(volumes:\)/\\1\\n$VOLUMES_HOSTPATH\\n$VOLUMES_HOSTPATH_PATH\\n$VOLUMES_HOSTPATH_TYPE\\n$VOLUMES_NAME/g" \
    -i $KUBE_API_SERVER

KKA_CLOUD_PROVIDER="--cloud-provider=openstack"
KKA_CLOUD_CONFIG="--cloud-config=$CC"
sed -e "s/\(Environment=\"KUBELET_KUBECONFIG_ARGS=.*\)\"/\\1 $KKA_CLOUD_PROVIDER $KKA_CLOUD_CONFIG\"/g" \
    -i $KUBE_ADM_CONF

systemctl daemon-reload
systemctl restart kubelet

for i in `seq 1 $N_WORKERS`; do
    scp_root_file_on_worker $CLOUD_CONF "$(($i - 1))" $CLOUD_CONF
    scp_root_file_on_worker $KUBE_ADM_CONF "$(($i - 1))" $KUBE_ADM_CONF
    execute_on_worker_sudo "$(($i - 1))" "systemctl daemon-reload"
    execute_on_worker_sudo "$(($i - 1))" "systemctl restart kubelet"
done

kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-rbac.yaml && echo "Traefik-RBAC correctly applied" || echo "Problem while applying Traefik-RBAC"
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-ds.yaml && echo "Traefik-DS correctly applied" || echo "Problem while applying Traefik-DS"

echo "################ End of Giovanni Ciatto's initialisation script ################" ; echo
