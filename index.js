var express = require('express');
var app = express();

var startTime = new Date()
var counter = 0

app.get('/', function (req, res) {
  res.send('[Replica ' + startTime.toISOString() + '] Visits: ' + counter++);
});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});