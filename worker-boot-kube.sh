#!/bin/bash

# https://kubernetes.io/docs/setup/independent/install-kubeadm/
USERNAME=ubuntu
USER_HOME=/home/$USERNAME
KUBEADM_INIT_FILE=$USER_HOME/kubeadm.log.txt
HOSTNAME=`hostname`

function log {
    echo "    [GCis] $1"
}

echo ; echo "################### Giovanni Ciatto's initialisation script ####################"

echo "127.0.0.1		$HOSTNAME" >> /etc/hosts

apt-get -qq update
apt-get -qq  install -y docker.io avahi-daemon avahi-discover avahi-utils libnss-mdns mdns-scan \
    && log "Docker & mDNS correctly installed" \
    || log "Problem while installing Docker or mDNS"

apt-get -qq update && apt-get -qq install -y apt-transport-https curl \
    && log "HTTP utils correctly installed" \
    || log "Problem while installing HTTP utils"

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF

KUBE_VERSION=1.11.3-00

apt-get -qq update
apt-get -qq install -y kubelet=$KUBE_VERSION kubeadm=$KUBE_VERSION kubectl=$KUBE_VERSION \
    && log "Kube* correctly installed" \
    || log "Problem while installing Kube*"
apt-mark hold kubelet kubeadm kubectl

kubeadm config images pull \
    && log "Checking docker images ok" \
    || log "Problems with kube* docker images"

echo "################ End of Giovanni Ciatto's initialisation script ################" ; echo
