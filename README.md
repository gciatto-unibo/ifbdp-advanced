Infrastructures for Big Data Processing - Advanced
==================================================

[Giovanni Ciatto](mailto:giovanni.ciatto@unibo.it)'s final relation

Web version of this document available [here](https://gitlab.com/gciatto-unibo/ifbdp-advanced/blob/master/REAME.md).

## Exam 3) Deploy a load balanced cluster of Wordpress instances

- Wordpress (https://wordpress.org) is a very popular blogging and website content management system based on PHP and MySQL

- What you should do:

  + Install Wordpress on `Cloud@CNAF` using containers

  + Deploy the setup via Kubernetes using Minikube, see https://kubernetes.io/docs/setup/minikube/

  + Implement load balancing of multiple Wordpress instances, explaining how you did it, why you chose a certain solution and show that the setup actually load balances Wordpress requests
  
  + Write a final report describing the goals of your project, the general architecture,
your setup and the relevant activities and solutions adopted to achieve it

- Reference person: Barbara Martelli ([barbara.martelli@cnaf.infn.it](mailto:barbara.martelli@cnaf.infn.it))

### Part 1: Docker

* Wordpress is a classical multi-tier app
* Instantiate a Wordpress application using containers (Docker or udocker) inside your `Cloud@CNAF` VM
  - Typically, you deploy a Wordpress container and a MySQL container and link them together
  - Use Docker Hub in order to find useful containers
* What happens if you stop and restart one of the containers?
* What happens if you restart the host?
* Are you able to scale up the application tier?

### Part 2: Kubernetes

- Try the same exercise using a kubernetes cluster (use Minikube)
- In order to share data between pods in a persistent way (so that data
survives to a pod restart) you’ll need to use some kind of `PersistentVolume`
  + Note that in minikube you have only the hostpath option. A volume survives after Pod restart, but gets reset in case of node reboot
- You can find an example here: https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/
- How can you achieve load balancing?

<!-- ### Notes

We agreed with Barbara Martelli to deviate from the Exam Track in the following way.

We will try to configure a cluster of virtual machines to act as Docker Swarm cluster, and we will then deploy a replicated Wordpress service on top of such a cluster.

We will try to configure a cluster of virtual machines to act as Kubernetes cluster, thus avoiding the need for Minikube, and we will then deploy a replicated Wordpress service on top of such a cluster. -->

## Designing a solution

After studying both Kubernetes and Docker Swarm, I realised they essentially share some common traits.
For instance they can both be installed on top of a cluster of (virtual) machines by means of two very similar procedures.
After doing so, both Kubernetes and Docker Swarm can work as _container orchestrators_, automatically assigning containers to machines.
This is of particular interest in the case of a replicated containerised application where replicas may be actually spreaded on different machines in order to improve the application robustness, availability, and maintanibility.

When deploying Kubernetes or Docker Swarm on a cluster of machines one usually need to select one machine to act as the Kubernetes/Docker Swarm __master__ machine.
The other machines will play the role of Kubernetes/Docker Swarm **slave**s.

In order to achieve such a configuration, one must run a specific _initialisation command_ (`kubeadm init` in case of Kubernetes or `docker swarm init` in case of Docker Swarm), on the machine that has been selected as the cluster master.
The output of such an initialisation command will contain the specific command to be executed on _each_ machine in order for it to become a Kubernetes/Docker Swarm slave. 

Thanks to the skills we acquired during the IfBDP-Basic course, we can easily deploy a Master-Worker cluster on top of `Cloud@CNAF`, analogous to the one we deployed for the IfBDP-Basic course.

The initialisation scripts for such cluster will need to:

- install Docker Swarm or Kubernetes on all machines, and

- configure the many instances of Docker Swarm/Kubernetes to interact with each other in a master-slave fashion

<!-- In what follows, in order to produce a uniform discourse for both Docker and Kubernetes, and in order to avoid overloading the term "cluster", we will adhere to the following convention:
- we will use the "swarm" term to refer to the set of -- possibly interacting -- Docker Swarm or Kubernetes instances, whereas
- we will call "cluster" the set of (virtual) machines hosting the Docker Swarm or Kubernetes instances. -->

Of course, there is a 1-to-1 correspondence between the Docker Swarm (resp. Kubernetes) instances and the cluster machines, in the sense that each Docker Swarm (resp. Kubernetes) instance is actually being run on a different machine and no two Docker Swarm (resp. Kubernetes) instances are being run on the same machine.

<!-- This convention is expected to be natural for Docker Swarm users but also astonishing for Kubernetes users.
In fact, Kubernetes users usually employ the term "cluster" to refer to both the set of machines and the set of Kubernetes instances. -->

In particular, we will call `master-node` the machine hosting the _master_ instance of a Docker Swarm or Kubernetes setup, whereas we will call `worker-<i>-node` the machine hosting the `i`-th _slave_ instance.

Both Docker Swarms and Kubernetes installations can be set up by means of the following abstract steps, which assumes all machines in the cluster to be up, running a properly configured by installing the proper Docker Swarm or Kubernetes software.
 
1. Setting up a cluster of (virtual) machines

0. Choose which machine should play the master role, say `master-node`

0. Initialise the Docker Swarm / Kubernetes master instance on top of `master-node`
    - This step provides as output a token `T` to be used by the slave instances running on top of the `worker-<i>-node` machines to join the swarm

0. Log in on all the `worker-<i>-node` machines and initialise a Docker Swarm / Kubernetes slave instance per machine, by means of token `T`

All the above steps can be performed by means of initialisation scripts to be run by the HEAT engine upon the first start up of each virtual machine composing the cluster.

Once the Docker Swarm / Kubernetes setup has been completed, a replicated Wordpress service can be deployed on the swarm by issuing a `.yaml` file to the Docker Swarm / Kubernetes master.

The following sections describe how to realise the aforementioned abstract steps both in the case of Docker Swarm (requirements part 1) and Kubernetes (requirements part 2).

Of course, despite being initially very similar, the two configuration processes (one for Docker Swarm and the other for Kubernetes) will eventually diverge.
This will happen because we decided to configure Kubernetes in a finer way.
In particular, we decided to make Kubernetes aware of the fact that it is running on top of an OpenStack-powered virtual infrastructure.
This in turn enables Kubernetes instances running on top of the virtual machines to store the containerised application data -- namely, the Wordpress database -- on the volumes possibly defined within the scope of the OpenStack tenant.
Furthermore, we will configure Kubernetes in such a way it can expose the replicated services it will host -- again, Wordpress -- on the Internet.
To do so, we will need to add the [`Traefik` plugin](https://docs.traefik.io/user-guide/kubernetes/) to our Kubernetes master instance.

## Setting up the cluster

Both Docker Swarm and Kubernetes require a cluster to be set up on `Cloud@CNAF`.
This can be achieved by means of the `ifbdp-docker.yaml` or `ifbdp-kube.yaml` HEAT templates, respectively.

The `ifbdp-docker.yaml` or `ifbdp-kube.yaml` scripts essentially share the same structure and actually produce the same infrastructure if provided as input to the HEAT engine.
They automatically set up a cluster composed by `N + 1` machines, one acting as the `master-node` whereas the other `N` act as the `worker-<i>-node`s.
Such machines are part of the same sub-network (`192.168.1.XXX`), where no limitation is imposed on the TCP/UDP ports which can be exposed by each machine.
This is required in order for the `N+1` Docker/Kubernetes instances to freely communicate.
The sub-network is connected to the Internet by means of a router which only allows incoming connections by means of the HTTP(S) and SSH protocols, on their well-known ports.
A public and floating IP is assigned to the `master-node`, making it accessible from the Internet.

Differently from the IfBDP-Basic exam, here, the HEAT template will also generate a novel public/private-key pair to be employed by the `master-node` in order to securely execute commands on each `worker-<i>-node` by means of `ssh`.
This is required because, in order to implement the configuration steps described into the previous section, the `master-node` will eventually need to invoke the join command -- which in turn is generated by the Docker/Kubernetes initialisation command, executed on the `master-node` -- on _each_ worker node.

Furthermore, the HEAT template two volumes, namely `shared-volume-1` and `shared-volume-2`, should be created, even if they are not attached to any machine.
This is deliberate and it is necessary in the case Kubernetes is chosen as an orchestrator, in order for Kubernetes-deployed services to store data persistently.

Notice that the `master-node` initialisation depends on the `worker-<i>-node`s ones, according to the `ifbdp-docker.yaml` or `ifbdp-kube.yaml` templates.
This means it should start only after the `worker-<i>-node`s ones terminated.
Such a constraint is necessary since the `master-node` initialisation script will need to execute a `join` command on all the `worker-<i>-node` machines after opening a `ssh` session towards such machines, so _all_ `worker-<i>-node` machines must be up, running, and properly configured when the `master-node` initialisation will attempt to contact them.

### Setup tutorial

The present subsection describes the common steps required to set up a Docker Swarm or Kubernetes configuration according to our proposed procedure.

First of all, the administrator user's system is assumed to be properly configured.
This assumes the `python-openstackclient` and `python-heatclient` Python packages to be installed on the user's machine and the OpenStack environment variables to be properly setup as well.

In order to attain the aforementioned configuration, the user may run the following commands:

```bash
# Install Openstack's CLI client
[sudo] pip[3] install python-openstackclient
# Install HEAT client
[sudo] pip[3] install python-heatclient

# Set OpenStack environment variables
export OS_AUTH_URL=https://horizon.cloud.cnaf.infn.it:5000/v3
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_ID=ff4a782c94cd47fd92b7a254fcc02efd
export OS_REGION_NAME=regionOne
export OS_USER_DOMAIN_NAME=Default
export OS_USERNAME=guest01
export OS_PASSWORD=#guest01's password here
```

Finally, one may instantiate the virtual cluster by running the following command:
```bash
openstack stack create --template ifbdpa-<either docker or kube here>.yaml \
  --parameter keyname=<key here> \
  --parameter os-password=<guest01 password here> \
  <stack name here>
```
where `ifbdpa-` must be followed by either `kube` or `doker` and the only two parameters which need to be explicitly provided by the user are the one coming with no smart default in `ifbdpa-*.yaml`:

- __`keyname`:__ is the name of the key which will be given access to the `master-node` by means of `ssh`.
It is assumed to be already registered on OpenStack and the user is assumed to hold the private key

- __`os-password`__: is the `guest01` user's password which is needed by the Kubernetes master to interact with the OpenStack tenant hosting it.
Of course, this parameter only makes sense in case of a Kubernetes setup.

### The initialisation scripts

We recall the `ifbdpa-$ORCHESTRATOR.yaml` -- where `$ORCHESTRATOR` may be either `docker` or `kube` -- states that `N+1` machines should actually be deployed and inter-connected by means of a LAN.
The `master-node` machine is configured by means of the `master-boot-$ORCHESTRATOR.sh` initialisation script, while the other `N` machines `worker-<i>-node` will be configured by means of the `worker-boot-$ORCHESTRATOR.sh` initialisation script.
Such initialisation scripts take care of installing the required software and jointly configuring Docker Swarm / Kubernetes on top of the cluster.
Here we describe the part of such scripts which is equal for both the master and the worker machines, regardless of the value of `$ORCHESTRATOR`.

1. The `master-boot-$ORCHESTRATOR.sh` and `worker-boot-$ORCHESTRATOR.sh` scripts, in all their variants, rely on the following environment variables:
    ```bash
    USER_NAME=ubuntu            # the name of the master-worker's user loggable by means of ssh
    USER_HOME=/home/$USER_NAME  # its home directory
    HOSTNAME=`hostname`         # the current machine's hostname (one of `master-worker` or `worker-<i>-node`, for i=1..N)
    ```

2. They also exploits the following utility functions:
    ```bash
    # Makes initialisation-related logs easier to spot because of intentation
    #     $1 -- is the string to be logged
    function log {
        echo "    [GCis] $1"
    }

    # Executes the command $2 as root on `worker-$1-node`
    #     $1 -- is the index of the worker node the command should be executed upon
    #     $2 -- is the command (along with its argument) to be run on `worker-$1-node`
    function execute_on_worker_sudo {
        echo $"ssh -oStrictHostKeyChecking=no \"$USER_NAME@worker-$1-node.local\" \"sudo\" $2"
        ssh -oStrictHostKeyChecking=no "$USER_NAME@worker-$1-node.local" "sudo" $2
    }
    ```
    Notice that `execute_on_worker_sudo` assumes all worker nodes are referenceable by means of the `worker-$1-node.local` host name.
    To ensure this pre-condition, mDNS is installed on all machines by means of the following command.
    mDNS (a.k.a. AVAHI) is a software to be installed one the many machines composig a LAN-based cluster in order for them to be easily referenceable by means of their __hostnames__.
    In practice, supposing mDNS is installed on all machines joining the same LAN, each machine `M` owns a LAN-wise domain name in the form `$(hostname).local` where `$(hostname)` is the hostname of `M`.
 
3. Then, some useful software can be installed by means of Ubuntu default repositories:
    ```bash
    apt-get -qq update

    # Install Docker, mDNS (Avahi) and IPVS
    apt-get -qq  install -y \
      docker.io=17.03.2-0ubuntu2~16.04.1 \ # we only tested this version
      avahi-daemon avahi-discover avahi-utils libnss-mdns mdns-scan \
      ipvsadm \
        && log "Docker & mDNS correctly installed" \
        || log "Problem while installing Docker or mDNS"

    # Install some HTTP utils, there including curl and w3m
    apt-get -qq update && apt-get -qq install -y \
      apt-transport-https \
      curl \
      w3m \
        && log "HTTP utils correctly installed" \
        || log "Problem while installing HTTP utils"
    ```
    Thanks to the installation of mDNS on all machines, each machine will be accessible from the other ones by means of its hostname.
    More precisely, suppose a machine local host name (i.e., the output of the `hostname` command) is `worker-<i>-node`, then any other machine may access it by means of the `worker-<i>-node.local` hostname, thus making the aforementioned `execute_on_worker_sudo` function work.

    The other software being installed is described below:
    
    - `docker.io`, i.e. the Docker daemon and client which are required by both Docker Swarm and Kubernetes
    - `IPVS` which is required by Docker Swarm to expose its services by means of virtual IPs
    - `curl` and `w3m` which may be employed to test the deployed services---e.g. Wordpress 

4. The subsequent configuration steps vary a lot depending on whether one is setting up a Docker Swarm or Kubernetes. 
So, we discuss them later in this document.

#### Docker Swarm

Here we describe how Docker Swarms is set up, i.e., how the aforementioned point 4 is implemented in case the `ifbdpa-docker.yaml` is employed.
We recall the `ifbdpa-docker.yaml` states that `N+1` machines should actually be deployed and inter-connected by means of a LAN.
The `master-node` machine is configured by means of the `master-boot-docker.sh` initialisation script, the other `N` machines `worker-<i>-node` will be configured by means of the `worker-boot-docker.sh` initialisation script.

4. Both in case of `master-boot-docker.sh` and `worker-boot-docker.sh`, assuming the Docker, IPVS, and mDNS installations were successfull, the machines need some further configuration steps:
    ```bash
    # Load the IPVS kernel module (in order to support virtual IPs, for load balancing)
    modprobe ip_vs 
      && log "IPVS modules loaded" 
      || log "Problem while loading IPVS module"

    # Start the Docker service
    service docker start 
      && log "Docker service started" 
      || log "Problem while starting the docker service"

    # Enables the machine user to interact with the docker service 
    usermod -a -G docker $USER_NAME 
      && log "$USER_NAME added to the docker group" 
      || log "Problem while adding $USER_NAME to the docker group"
    ```

From now on, the two scripts diverge.
More precisely, `worker-boot-docker.sh` have no more command to be executed, whereas the `master-boot-docker.sh` script must now actually configure Docker Swarm.

##### The master node

The `master-boot-docker.sh` script performs some further configuration steps, described below.

5. It assumes the following environment variable to be set:
    ```bash
    DOCKER_SWARM_INIT_FILE=$USER_HOME/docker.log.txt
    ```
    It contains the path where the output of the next command will be stored.

6. Then, the master instance of Docker is initialised by means of the following command:
    ```bash
    docker swarm init > $DOCKER_SWARM_INIT_FILE 
      && log "Docker swarm correctly initialised" 
      || log "Problem while initialising docker swarm"
    ```
    If the command above executes correctly, the `$DOCKER_SWARM_INIT_FILE` file should contain some text similar to the following one:
    ```
    To add a worker to this swarm, run the following command:
        docker swarm join \
        --token SWMTKN-1-5bkgbdn3xezdlp8rhndhymmisfria56fgzyq9r483ihyqs72hb-9twoahqktg2mvy6jepryo5e8t \
        192.168.1.4:2377
    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
    ``` 
    where the indented lines (4 spaces) represent __the command to be run on each worker machine in order to make it join the current swarm__. 
    
7. The `master-node` must thus execute such command on each worker node. 
To do so, it can rely on the following assumptions:

- the worker nodes should have alredy completed their initialisation

- the environment variable `$PRIVATE_KEY` contains the private key needed to connect to each worker node by means of `ssh`

    The `master-node` will then configure on the fly `ssh` to use such a private key:
    ```bash
    echo "$PRIVATE_KEY" >> $USER_HOME/.ssh/id_rsa
    chown $USER_NAME:$USERNAME $USER_HOME/.ssh/id_rsa
    chmod 400 $USER_HOME/.ssh/id_rsa
    echo "$PRIVATE_KEY" >> ~/.ssh/id_rsa
    chmod 400 ~/.ssh/id_rsa
    ```

8. It then prepares the command to be executed on each worker machine by filtering out non-indented lines from the `$DOCKER_SWARM_INIT_FILE` file
    ```bash
    JOIN_CMD=`cat $DOCKER_SWARM_INIT_FILE | grep "    "`
    #                                             ^^^^  4 spaces
    ```

    The `JOIN_CMD` variable should now contain the following string:
    ```
    docker swarm join \
    --token SWMTKN-1-5bkgbdn3xezdlp8rhndhymmisfria56fgzyq9r483ihyqs72hb-9twoahqktg2mvy6jepryo5e8t \
    192.168.1.4:2377
    ```
 
9. Finally, it executes the `$JOIN_CMD` command on each worker machine by means of the `execute_on_worker_sudo` function:
    ```bash
    for i in `seq 1 $N_WORKERS`; do
        execute_on_worker_sudo "$(($i - 1))" "$JOIN_CMD"
    done
    ```
    where `$N_WORKERS` is a variable storing the total amount of worker nodes which have been deployed, whose value is actually provided by the HEAT engine, before the script is even executed.

#### Kubernetes

Here we describe how Kubernetes is set up, i.e., how the aforementioned point 4 is implemented in case the `ifbdpa-kube.yaml` is employed.
We recall the `ifbdpa-kube.yaml` states that `N+1` machines should actually be deployed and inter-connected by means of a LAN.
The `master-node` machine is configured by means of the `master-boot-kube.sh` initialisation script, the other `N` machines `worker-<i>-node` will be configured by means of the `worker-boot-kube.sh` initialisation script.

The following steps are inspired to these tutorials: 

- https://kubernetes.io/docs/setup/independent/install-kubeadm/ 

- https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

4. Both in case of `master-boot-kube.sh` and `worker-boot-kube.sh`, assuming the Docker and IPVS installations were successfull, the machines need some further configuration steps:
    ```bash
    # Add the signature for the Kubernetes repository
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    # Add the Kubernetes repository to the local Ubuntu system
    cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
    deb http://apt.kubernetes.io/ kubernetes-xenial main
    EOF

    # Variable aimed at constraining the Kubernetes version
    KUBE_VERSION=1.11.3-00

    # Install Kubernetes (i.e., kubelet, kubeadmn, and kubectl)
    apt-get -qq update
    apt-get -qq install -y kubelet=$KUBE_VERSION kubeadm=$KUBE_VERSION kubectl=$KUBE_VERSION \
        && log "Kube* correctly installed" \
        || log "Problem while installing Kube*"

    # Prevents kubelet, kubeadmn, and kubectl from being updated
    apt-mark hold kubelet kubeadm kubectl
    ```
    Notice that a Kubernetes installation consists of three packages and their respective commands, which need to co-exist at the __exact same version__:
      - `kubeadm`: the command to bootstrap the Kubernetes cluster
      - `kubelet`: the component that runs on all of the machines in your cluster and is in charge of things like starting pods and containers
      - `kubectl`: the command line utility to manage the Kubernetes cluster

5. The next common step consists of checking whethe the Docker images required by Kubernetes for its normal operations are available:
    ```bash
    kubeadm config images pull \
      && log "Checking docker images ok" \
      || log "Problems with kube* docker images"
    ```

From now on, the two scripts diverge.
More precisely, `worker-boot-kube.sh` have no more command to be executed, whereas the `master-boot-kube.sh` script must now actually configure the Kubernetes cluster.

##### The master node

The `master-boot-kube.sh` script performs some further configuration steps, described below and strongly inspired by the following tutorials: 

- https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

- https://medium.com/@arthur.souzamiranda/kubernetes-with-openstack-cloud-provider-current-state-and-upcoming-changes-part-1-of-2-48b161ea449a

6. Firstly, it assumes the following environment variables to be set:
    ```bash
    # File where the join command to be executed on the worker machines will be stored
    KUBEADM_INIT_FILE=$USER_HOME/kubeadm.log.txt 
    # Configuration directory for Kubernetes
    ETC_KUBE=/etc/kubernetes 
    # The configuration file storing the enviroment info required by Kubernetes to interact with the cloud provider hosting it
    CLOUD_CONF=$ETC_KUBE/cloud.conf
    # Configuration directory for Kubernetes manifests files
    ETC_KUBE_MANIFESTS=$ETC_KUBE/manifests
    # Two configuration files stating -- among the many information -- where to look for persistent volumes
    KUBE_CONTROLLER_MANAGER=$ETC_KUBE_MANIFESTS/kube-controller-manager.yaml
    KUBE_API_SERVER=$ETC_KUBE_MANIFESTS/kube-apiserver.yaml
    # Configuration file stating which enviroment variables should be defined on `kubelet` startup
    KUBE_ADM_CONF=/etc/systemd/system/kubelet.service.d/10-kubeadm.conf
    ```
    Thay contain the paths of some important Kubernetes configuration files which will be edited by the following steps, namely the files `$KUBE_CONTROLLER_MANAGER`, `$KUBE_API_SERVER`, and `KUBE_ADM_CONF`.
    
    In particular, `$KUBEADM_INIT_FILE` stores the path where the output of the next command will be stored.

7. The `kubeadm` utility is then used to initialise the Kubernetes master on the `master-node`, by executing:
    ```bash
    # initialise
    kubeadm init --pod-network-cidr=10.244.0.0/16 > $KUBEADM_INIT_FILE

    # the owner of the $KUBEADM_INIT_FILE file should be $USER_NAME
    chown $USER_NAME:$USER_NAME $KUBEADM_INIT_FILE
    # and it should be readable by any user
    chmod 444 $KUBEADM_INIT_FILE
    ```
    The `--pod-network-cidr=10.244.0.0/16` argument is required since, in the following, we are going to setup some network pod add-on -- [Flannel](https://github.com/coreos/flannel) in particular -- which requires it.  

    The output of the `kubeadm init` command -- and therefore the content of the `$KUBEADM_INIT_FILE` file -- should be something in the form:
    ```
    Your Kubernetes master has initialized successfully!

    To start using your cluster, you need to run (as a regular user):

        mkdir -p $HOME/.kube
        sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
        sudo chown $(id -u):$(id -g) $HOME/.kube/config

    You should now deploy a pod network to the cluster.
    Run "kubectl apply -f [podnetwork].yaml" with one of the addon options listed at:
    http://kubernetes.io/docs/admin/addons/

    You can now join any number of machines by running the following on each node
    as root:

        kubeadm join --token <token> <master-ip>:<master-port> --discovery-token-ca-cert-hash sha256:<hash>
    ```
    where the last line contains the command to be executed on the worker nodes in order for them to join the master.

8. As suggested by the outcome of the `kubeadm init` command above, we need to configure the environment for both `root` and the regular users such as `$USER_NAME`:
    ```bash
    mkdir -p $USER_HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $USER_HOME/.kube/config
    sudo chown $USER_NAME:$USER_NAME $USER_HOME/.kube/config
    
    export KUBECONFIG=/etc/kubernetes/admin.conf
    ```

9. Then, as described in https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#tabs-pod-install-4, the following commands must be executed in order for the [Flannel network pod addon](https://github.com/coreos/flannel) to be installed:
    ```bash
    sysctl net.bridge.bridge-nf-call-iptables=1

    kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml  
        && echo "Flannel correctly applied" 
        || echo "Problem while applying Flannel"
    ```
    According to our current understanding of the Kubernetes functioning, a network pod addon is needed to let the many pods possibly spreaded by Kubernetes all over the cluster communicate by means of an overlay network.

10. As in the Docker Swarm case above, the worker machines should now join the Kubernetes master. 
To do so, the `master-node-kube.sh` script configures `ssh` on the fly in order to easily execute commands on them:
    ```bash
    echo "$PRIVATE_KEY" >> $USER_HOME/.ssh/id_rsa
    chown $USER_NAME:$USERNAME $USER_HOME/.ssh/id_rsa
    chmod 400 $USER_HOME/.ssh/id_rsa
    echo "$PRIVATE_KEY" >> ~/.ssh/id_rsa
    chmod 400 ~/.ssh/id_rsa
    ``` 

11. It then prepares the command to be executed on each worker machine by filtering from the `$KUBEADM_INIT_FILE` file a line containing `"kubeadm join"`:
    ```bash
    JOIN_CMD=`cat $KUBEADM_INIT_FILE | grep "kubeadm join"`
    ```
    The `JOIN_CMD` should now contain a string in the form
    ```bash
    kubeadm join --token <token> <master-ip>:<master-port> --discovery-token-ca-cert-hash sha256:<hash>
    ```

12. It then executes the `$JOIN_CMD` command on each worker machine by means of the `execute_on_worker_sudo` function:
    ```bash
    for i in `seq 1 $N_WORKERS`; do
        execute_on_worker_sudo "$(($i - 1))" "$JOIN_CMD"
    done
    ```
    where `$N_WORKERS` is a variable storing the total amount of worker nodes which have been deployed, whose value is actually provided by the HEAT engine, before the script is even executed.

The purposes of the subsequent steps are to configure the Kubernetes cluster in order for it to be able to connect to the OpenStack service hosting it (i.e., `OpenStack@CNAF`).
This, in turn, is required since we want Kubernetes to exploit the OpenStack volumes mentioned in the previoud section (i.e., `shared-volume-1` and `shared-volume-2`) for its persistent volumes provisioning.
Analogous configuration steps may be required in order for Kubernetes to exploit OpenStack's _Load Balancing as a Service_ (LBaaS) facilities as well.
Sadly, this latter feature is currently not supported by `OpenStack@CNAF` and it won't therefore be described in this document.

In order to make Kubernetes able to interact with `OpenStack@CNAF`, the following files need to be created or edited on the `master-node` or on the other nodes:

- `/etc/kubernetes/cloud.conf` to be __created__ on __all__ machines in order to inform Kubernetes about the OpenStack tenant's credentials and API entrypoint

- `/etc/kubernetes/manifests/kube-controller-manager.yaml` and `/etc/kubernetes/manifests/kube-apiserver.yaml` to be __edited__ on the `master-node` __only__ in oder to let Kubernetes know where to look for persistent volumes

- `/etc/systemd/system/kubelet.service.d/10-kubeadm.conf` to be __edited__ on __all__ machines in order to let Kubernetes know which cloud provider it should interact with

According to the requirements of this project, a persistent volume is needed because Wordpress lies on top of a Database (e.g. MySql), and the Database in turn requires some persistent volume where to store data into.

13. So the next step in the `master-node-kube.sh` script is to generate the `$CLOUD_CONF` on the `master-node`, as stated by this tutorial: https://medium.com/@arthur.souzamiranda/kubernetes-with-openstack-cloud-provider-current-state-and-upcoming-changes-part-1-of-2-48b161ea449a 
    ```bash
    generate_cloud_conf $CLOUD_CONF
    ```
    Where the `generate_cloud_conf` function's code is shown below:
    ```bash
    function generate_cloud_conf {
        echo '[Global]'                                 >  $1
        echo "username=$OS_USERNAME"                    >> $1
        echo "password=$OS_PASSWORD"                    >> $1
        echo "auth-url=$OS_AUTH_URL"                    >> $1
        echo "tenant-id=$OS_TENANT_ID"                  >> $1
        echo "domain-name=$OS_DOMAIN_NAME"              >> $1
        echo "region=$OS_REGION"                        >> $1
        echo                                            >> $1
        echo '[LoadBalancer]'                           >> $1
        echo "subnet-id=$SUBNET_ID"                     >> $1
        echo "floating-network-id=$FLOATING_NETWORK_ID" >> $1
    }
    ```
    Notice that, as stated above, `CLOUD_CONF=/etc/kubernetes/cloud.conf`.
    Also notice that the aforementioned variables are indeed substituted with their acutal values by the Heat engine.
    The actual effect of the procedure above is to generare the /etc/kubernetes/cloud.conf with the following content:
    ```
    [Global]
    username=guest01
    password=#quest 1 password
    auth-url=https://horizon.cloud.cnaf.infn.it:5000/v3
    tenant-id=ff4a782c94cd47fd92b7a254fcc02efd
    domain-name=Default
    region=regionOne

    [LoadBalancer]
    subnet-id=32fa2108-ae03-4b0a-8757-6285764539ac
    floating-network-id=17d5949b-5694-4eab-abe0-c8039f0bd498
    ```

    In a few steps, the so-ganerated `$CLOUD_CONF` file will be copied on all worker machines.

14. Then, the `master-node-kube.sh` script must edit the `master-node`'s `$KUBE_CONTROLLER_MANAGER` file by adding the following lines:
    ```yaml
    [...] 

    spec:
      containers:
        - command:
          - kube-controller-manager    
          - --cloud-provider=openstack                  # add this line
          - --cloud-config=/etc/kubernetes/cloud.conf   # add this line
          [...]
        [...]

    [...]
      volumeMounts:
        - mountPath: /etc/kubernetes/cloud.conf         # add this line
          name: cloud-config                            # add this line
          readOnly: true                                # add this line

    [...]
      volumes:
        - hostPath:                                     # add this line
            path: /etc/kubernetes/cloud.conf            # add this line
            type: FileOrCreate                          # add this line
          name: cloud-config                            # add this line
        [...]

    [...]
    ```
    The same operation must be performed for the `$KUBE_API_SERVER` file on the `master-node`
    ```yaml
    [...] 

    spec:
      containers:
        - command:
          - kube-apiserver
          - --cloud-provider=openstack                  # add this line
          - --cloud-config=/etc/kubernetes/cloud.conf   # add this line
          [...]
        [...]

    [...]
      volumeMounts:
        - mountPath: /etc/kubernetes/cloud.conf         # add this line
          name: cloud-config                            # add this line
          readOnly: true                                # add this line

    [...]
      volumes:
        - hostPath:                                     # add this line
            path: /etc/kubernetes/cloud.conf            # add this line
            type: FileOrCreate                          # add this line
          name: cloud-config                            # add this line
        [...]

    [...]
    ```

    To do so, we extensively adopted the `sed` command, and a lot of supporting variables:
    ```bash
    PREFIX1='    -'
    PREFIX2='     '
    PREFIX3='  -'
    PREFIX4='   '
    CC=`echo $CLOUD_CONF | sed -e "s/\//\\\\\\\\\//g"` # replaces slashes with double-slashes in $CLOUD_CONF
    COMMAND_CLOUD_PROVIDER="$PREFIX1 --cloud-provider=openstack"
    COMMAND_CLOUD_CONFIG="$PREFIX1 --cloud-config=$CC"
    VALUME_MOUNTS_PATH="$PREFIX1 mountPath: $CC"
    VALUME_MOUNTS_NAME="$PREFIX2 name: cloud-config"
    VALUME_MOUNTS_RO="$PREFIX2 readOnly: true"
    VOLUMES_HOSTPATH="$PREFIX3 hostPath:"
    VOLUMES_HOSTPATH_PATH="$PREFIX2 path: $CC"
    VOLUMES_HOSTPATH_TYPE="$PREFIX2 type: FileOrCreate"
    VOLUMES_NAME="$PREFIX4 name: cloud-config"

    # inserts lines in $KUBE_CONTROLLER_MANAGER
    sed -e "s/\(- kube-controller-manager\)/\\1\\n$COMMAND_CLOUD_PROVIDER\\n$COMMAND_CLOUD_CONFIG/g" \
        -e "s/\(volumeMounts:\)/\\1\\n$VALUME_MOUNTS_PATH\\n$VALUME_MOUNTS_NAME\\n$VALUME_MOUNTS_RO/g" \
        -e "s/\(volumes:\)/\\1\\n$VOLUMES_HOSTPATH\\n$VOLUMES_HOSTPATH_PATH\\n$VOLUMES_HOSTPATH_TYPE\\n$VOLUMES_NAME/g" \
        -i $KUBE_CONTROLLER_MANAGER

    # inserts lines in $KUBE_API_SERVER
    sed -e "s/\(- kube-apiserver\)/\\1\\n$COMMAND_CLOUD_PROVIDER\\n$COMMAND_CLOUD_CONFIG/g" \
        -e "s/\(volumeMounts:\\)/\\1\\n$VALUME_MOUNTS_PATH\n$VALUME_MOUNTS_NAME\\n$VALUME_MOUNTS_RO/g" \
        -e "s/\(volumes:\)/\\1\\n$VOLUMES_HOSTPATH\\n$VOLUMES_HOSTPATH_PATH\\n$VOLUMES_HOSTPATH_TYPE\\n$VOLUMES_NAME/g" \
        -i $KUBE_API_SERVER
    ```

    The `$KUBE_CONTROLLER_MANAGER` and `$KUBE_API_SERVER` just need to be edited on the `master-node`, so we are done with them.

    As a side note, the `sed` invocation above, must be interpreted according to the following pattern:
    ```bash
    sed -e "s/<replaced>/<replacer>/g" -i <filepath>
    ```
    where each `-e "s/<replaced>/<replacer>/g"` states that a replacement should be performed on file `<filepath>` in such a way that each occurrence of `<replaced>` is replaced by `<replacer>`.

15. One last file needs to be edited on all machines, that is, the `$KUBE_ADM_CONF` one, as stated by the https://medium.com/@arthur.souzamiranda/kubernetes-with-openstack-cloud-provider-current-state-and-upcoming-changes-part-1-of-2-48b161ea449a tutorial.
Again, it is edited on the master and it is then copied on the worker nodes. 
In particular, it must be edited in such a way that the `--cloud-provider=openstack` and `--cloud-config=$CLOUD_CONF` arguments are _appendend_ to the `KUBELET_CONFIG_ARGS` environment variable which is deckared in that file.
To do so, the following commands need to be executed, again involving `sed`:
    ```bash
    KKA_CLOUD_PROVIDER="--cloud-provider=openstack"
    KKA_CLOUD_CONFIG="--cloud-config=$CC"

    sed -e "s/\(Environment=\"KUBELET_KUBECONFIG_ARGS=.*\)\"/\\1 $KKA_CLOUD_PROVIDER $KKA_CLOUD_CONFIG\"/g" \
        -i $KUBE_ADM_CONF
    ```
    
    In a few steps, the new version of the `$KUBE_ADM_CONF` will be propagated to the worker machines.

16. The `kubelet` daemon should now be reloaded and restarted:
    ```bash
    systemctl daemon-reload
    systemctl restart kubelet
    ```

17. The last but one step consists of propagating the `$CLOUD_CONF` and `$KUBE_ADM_CONF` files to each `worker-<i>-node` machine.
To do so, the following loop is executed:
    ```bash
    for i in `seq 1 $N_WORKERS`; do
        scp_root_file_on_worker $CLOUD_CONF "$(($i - 1))" $CLOUD_CONF
        scp_root_file_on_worker $KUBE_ADM_CONF "$(($i - 1))" $KUBE_ADM_CONF
        execute_on_worker_sudo "$(($i - 1))" "systemctl daemon-reload"
        execute_on_worker_sudo "$(($i - 1))" "systemctl restart kubelet"
    done
    ```
    where `scp_root_file_on_worker` essentially copies the local `$CLOUD_CONF` file on the `worker-<i>-node` machine at path `$CLOUD_CONF`.
    Notice, that the loop also reloads & restarts the `kubelet` daemon on each worker machine.

    The code of the `scp_root_file_on_worker` function is presented below:
    ```bash
    # copies $1 on worker-$2-node at path $3 and ensures the copy owner to be root
    function scp_root_file_on_worker {
        BASENAME=`basename $1`
        scp -oStrictHostKeyChecking=no "$1" "$USERNAME@worker-$2-node.local:$BASENAME"
        ssh -oStrictHostKeyChecking=no "$USERNAME@worker-$2-node.local" sudo chown root:root $BASENAME
        ssh -oStrictHostKeyChecking=no "$USERNAME@worker-$2-node.local" sudo cp $BASENAME $3
    }
    ```

18. Finally, the [Traefik](https://docs.traefik.io/) plugin for Kubernetes need to be applied in order for the upcoming Wordpress service -- or any other service being deployed on the cluster -- to be exposed to the Internet.
To do so, we need to execute the following commands:
    ```bash
    kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-rbac.yaml && echo "Traefik-RBAC correctly applied" || echo "Problem while applying Traefik-RBAC"
    kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-ds.yaml && echo "Traefik-DS correctly applied" || echo "Problem while applying Traefik-DS"
    ```

The configuration process should now be over.

Users can now check if the Kubernetes cluster is correctly configured by opening an `ssh` shell on the `master-node` by means of its floating IP, and by running the following command:
     ```bash
    kubectl get nodes
    ```
    If `N+1` nodes are displayed, then the cluster is properly configured.

## Setting up Wordpress

Now that we have a running Docker Swarm / Kubernetes cluster, we can deploy a Wordpress stack.

Regardeless of the orchestrator of choice, a Wordpress stack consists of the following layers:
1. A Wordpress service (possibly including one or more replicas of the Wordpress application) exposing the virtual port 80 (on all replicas)
2. A MySQL (or MariaDB) service (including just one replica, since it is statefull), possibly connected to some persistent volume, exposing the virtual port 3306

In the following subsections, we show how a stack of such a sort can be deployed on either a Docker Swarm or a Kubernetes cluster.
There, we assume that all `.yaml` files from [this project repository](https://gitlab.com/gciatto-unibo/ifbdp-advanced) have been copied on the `master-node`:
```bash
scp *.yaml ubuntu@<master node floating IP>:
```
and that a `ssh` shell have been opened on that machine:
```bash
ssh ubuntu@<master node floating IP>
```

### Part 1 -- Docker

Assuming the Docker Swarm is correctly configured and running, deploying a Wordpress stack is as simple as executing the following command:
```bash
docker stack deploy -c docker_stack-wordpress.yaml wp
```
where `wp` is the name of the to-be-deployed stack, and `docker_stack-wordpress.yaml` contains the following specification
```yaml
version: '3.0'

services:
  db:
    # the db service will be based of the mysql:5.7 image
    image: mysql:5.7 
    volumes:
      # mounts the db_data volume (defined below) tp the /var/lib/mysql path on the container
      - db_data:/var/lib/mysql 
    deploy:
      # the db service will be backed by just one container
      replicas: 1
      # Docker will have to restart the container composing the service whenever it terminates
      restart_policy:
        condition: on-failure
    networks:
      # The service will be part of the `webnet` overlay network defined below
      - webnet
    environment:
      # Configures the username and passwords needed by wordpress to interact with MySQL
      MYSQL_ROOT_PASSWORD: wordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress_user
      MYSQL_PASSWORD: wordpress
      # !Notice that real world scenarios should employ secrets here

  wordpress:
    depends_on:
      # The wordpress service should start only after the db was successfully deployed
      - db
    # the db service will be based of the wordpress:latest image
    image: wordpress:latest
    ports:
      # the wordpress service will be avaiable on the host's 8080 port
      - 8080:80
    deploy:
      # the wordpress service will be (initially) replicated twice
      replicas: 2
      restart_policy:
        # Docker will have to restart the containers composing the service whenever they terminate
        condition: any
    networks:
      # The service will be part of the `webnet` overlay network defined below
      - webnet
    environment:
      # Environment variables storing the username, address, host and port needed to interact with MySQL
      WORDPRESS_DB_HOST: db:3306 # notice that the service name `db` is actually an hostname here
      WORDPRESS_DB_USER: wordpress_user
      WORDPRESS_DB_PASSWORD: wordpress
    
volumes:
  # this is where the `db_data` volume is defined
  db_data:
    # default settings for volumes are fine

networks:
  # this is where the `webnet` overlay network is defined
  webnet: 
    # default settings for networks are fine
```

The Wordpress service should now be __locally__ available on port 8080 on all machines composing the cluster, thanks to [Docker's routing mesh](https://docs.docker.com/engine/swarm/ingress/#publish-a-port-for-a-service).
Exposing it on the Internet is out of the scope of this project.

Notice that this deployment relies on no persistent volume.
If the machine hosting the DB container fails, and its data is corrupted, the DB data will be corrupted too.
So, the MySQL storage is volatile.
Again, configuring Docker volumes, is out of the scope of this project.

You can try your deployment by means of the `w3m` textual browser, by running:
```bash
w3m http://localhost:8080
```

W3M is a textual web browser.
So, if the command above shows a text-based representation of the Wordpress inizial wizard, then the Wordpress setup was successfull.

One can also ensure the stack was deployed successfully by means of the following command:
```bash
docker service ls
```
which should provide an output similar to the following one:
```
ID                  NAME                MODE                REPLICAS            IMAGE                                          PORTS
x38qagsnpvb5        wp_db               replicated          1/1                 mysql:5.7                                      
gsnizyqa92rm        wp_wordpress        replicated          2/2                 wordpress:latest                               *:8080->80/tcp
```

If the `REPLICAS` column shows that all replicas are up and running, then the Wordpress setup was successfull.

If everything went fine, the following command should eventually show a number of running containers
```bash
docker ps
```
along with their IDs, similarly to what is shown below
```
CONTAINER ID        IMAGE                                          COMMAND                  CREATED             STATUS              PORTS                 NAMES
a13cac25b003        mysql:5.7                                      "docker-entrypoint.s…"   18 seconds ago      Up 13 seconds       3306/tcp, 33060/tcp   wp_db.1.q2evapnhu2xa41boo74bnurfd
12c282c881d7        wordpress:latest                               "docker-entrypoint.s…"   30 seconds ago      Up 20 seconds       80/tcp                wp_wordpress.2.8mdaulmw8trhpf2z14479jf3a
9b31ee4b013b        wordpress:latest                               "docker-entrypoint.s…"   30 seconds ago      Up 20 seconds       80/tcp                wp_wordpress.1.a6bxl5ig4jit78ivbo0eiig0m
```
The `NAMES` columns is quite interesting since it adheres to the schema `<stackName>_<serviceName>.<replicaNum>.<replicaID>`.

If we now try to kill one of them, say `wp_wordpress.1.a6bxl5ig4jit78ivbo0eiig0m`, by running the following command,
```bash
docker kill wp_wordpress.1.a6bxl5ig4jit78ivbo0eiig0m
```
and then we keep re-executing the command
```bash
docker service ls
```
over and over again, we should notice that, even if the amount of replicas of one of the two services decreased,
```
ID                  NAME                MODE                REPLICAS            IMAGE                                          PORTS
x38qagsnpvb5        wp_db               replicated          1/1                 mysql:5.7                                      
gsnizyqa92rm        wp_wordpress        replicated          1/2                 wordpress:latest                               *:8080->80/tcp
```
it will eventually increase again, automatically:
```
ID                  NAME                MODE                REPLICAS            IMAGE                                          PORTS
x38qagsnpvb5        wp_db               replicated          1/1                 mysql:5.7                                      
gsnizyqa92rm        wp_wordpress        replicated          2/2                 wordpress:latest                               *:8080->80/tcp
```
Such a behaviour is due to Docker Swarm constantly attempting to steer the swarm towards a desired state, namely, the one there 2 replicas of the `wp_wordpress` service are up and running.

#### Scaling up a Docker service

Finally, the amount of replicas of the Wordpress service can be increased or decreased by means of the following command: 
```bash
docker service scale wp_wordpress=<amount of replicas>
```

If we scale the system up to `3` replicas and then we rapidly re-execute the 
```bash
docker service ls
```
command several times, we should initially note an output similar to the following one:
```
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
og9qb9yn5l3l        wp_db               replicated          1/1                 mysql:5.7
tie22r4kip00        wp_wordpress        replicated          2/3                 wordpress:latest    *:8080->80/tcp
```
which should eventually become like this:
```
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
og9qb9yn5l3l        wp_db               replicated          1/1                 mysql:5.7
tie22r4kip00        wp_wordpress        replicated          3/3                 wordpress:latest    *:8080->80/tcp
```
meaning that the scaling-up was successfull.

Of course, scaling down is as simple as running the same command as above specifying a lover number of replicas.

#### The load balancing

By performing the steps descrived above, Docker Swarm will expose the many replicas of the `wp_wordpress` service exposing them on the same port, namely `8080` according to the `yaml` specification shown above.
Docker Swarm also takes care of routing connections directed towards this port to the replicas according to some dynamic scheduling.
In practice Docker Swarm provides some native load balancing facilities.
This feature is known as [routing mesh](https://docs.docker.com/engine/swarm/ingress/) and its functioning is nicely represented by the following image:

![Docker Swarm's routing mesh feature](https://docs.docker.com/engine/swarm/images/ingress-routing-mesh.png)

where nodes are essentially the cluster machines, `my-web.<i>`s are the replicas of some web service `my-web`, whereas the _Ingress_ network is an overlay network created by Docker Swarm.

In order to test the behaviour of a routing mesh and understand how requests are routed to the many replicas of the `wp_wordpress` service, we built a Docker image, namely `gciatto/my-images:stateful-helloworld-server` -- available on DockerHub --, wrapping the following NodeJS web service:
```js
var express = require('express');
var app = express();

var startTime = new Date()
var counter = 0

app.get('/', function (req, res) {
  res.send('[Replica ' + startTime.toISOString() + '] Visits: ' + counter++);
});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});
```
It consists of a very minimal web service which will simply serve any HTTP request by providing a page showing the content of the `counter` variable, i.e., the total amount of requests served so far.
The web-service, upon start up will also save a timestamp into its `startTime` variable.
Such variable value will never be changed and can be conceived as an identifier for the web-service instance, within the scope of this test.
The actual response that is provided to the HTTP clients will expose both the `counter` variable and the `startTime` variable.
So the clients can essentially know _which_ particular web-service instance is serving them and how many requests it has served so far.

We now create another Docker stack, only containing one service, namely `shs`, made up of several replicas of the web-service described above which will listen for requests on port `8888`, as state by the following `yaml` specification:
```yaml
version: '3.0'

services:
  counter:
    image: "gciatto/my-images:stateful-helloworld-server"
    deploy:
      replicas: 10
      restart_policy:
        condition: on-failure
    ports:
      - 8888:8080
```
which is deployed by means of the following command:
```bash
docker stack deploy -c docker-statful-helloworld-server.yaml shs
```

Finally, we can demonstrate that requests are routed to different replicas by running the following command
```bash
for i in `seq 1 100`; do curl localhost:8888; echo ""; done
```
which will query the web-service 100 times.

If requests are actually load-balanced, the outcome of the previous loop should be something like:
```
[Replica 2018-10-14T07:15:10.058Z] Visits: 0
[Replica 2018-10-14T07:15:08.739Z] Visits: 0
[Replica 2018-10-14T07:15:08.576Z] Visits: 0
[Replica 2018-10-14T07:15:07.870Z] Visits: 0
[Replica 2018-10-14T07:15:07.367Z] Visits: 0
[Replica 2018-10-14T07:15:07.228Z] Visits: 0
[Replica 2018-10-14T07:15:07.042Z] Visits: 0
[Replica 2018-10-14T07:15:06.908Z] Visits: 0
[Replica 2018-10-14T07:15:05.615Z] Visits: 0
[Replica 2018-10-14T07:15:05.468Z] Visits: 0
[Replica 2018-10-14T07:15:10.058Z] Visits: 1
[Replica 2018-10-14T07:15:08.739Z] Visits: 1
[Replica 2018-10-14T07:15:08.576Z] Visits: 1
[Replica 2018-10-14T07:15:07.870Z] Visits: 1
[Replica 2018-10-14T07:15:07.367Z] Visits: 1
[Replica 2018-10-14T07:15:07.228Z] Visits: 1
[Replica 2018-10-14T07:15:07.042Z] Visits: 1
[Replica 2018-10-14T07:15:06.908Z] Visits: 1
[Replica 2018-10-14T07:15:05.615Z] Visits: 1
[Replica 2018-10-14T07:15:05.468Z] Visits: 1
[Replica 2018-10-14T07:15:10.058Z] Visits: 2
[Replica 2018-10-14T07:15:08.739Z] Visits: 2
...
```
proving that requests are indeed load balanced, appearently, according to some Round-Robin-like rule.

### Part 2 -- Kubernetes

Assuming the Kubernetes cluster is correctly configured and running, deploying a Wordpress stack and exposing it on the Internet requires a number of steps, since, in this case, we are mimicking a real-world deployment.

1. As a first step, we need to create a secret for the MySQL root password.
To do so, it is sufficient to execute the following command:
    ```bash
    kubectl create secret generic mysql-pass --from-literal=password=<secret here>
    ```

0. Then, one or more persisent volumes should be defined -- possibly backed by some actually persistent data storage -- in order for Kubernetes to dynamically provide volumes to its pods.
To do so, it is sufficient to create two resources of type `PersistentVolume` by means of the `cinder-storage.yaml` specification file---which must be updated to comprehend the IDs of the Cinder volumes instantiated by the HEAT template above:
    ```yaml
    apiVersion: v1
    kind: PersistentVolume
    metadata:
      name: pv-openstack-1
      labels:
        app: wordpress
    spec:
      storageClassName: cinder-volume
      capacity:
        storage: 20Gi
      accessModes:
        - ReadWriteOnce
      cinder: 
        fsType: ext3
        volumeID: # shared-storage-1
    ---
    apiVersion: v1
    kind: PersistentVolume
    metadata:
      name: pv-openstack-2
      labels:
        app: wordpress
    spec:
      storageClassName: cinder-volume
      capacity:
        storage: 20Gi
      accessModes:
        - ReadWriteOnce
      cinder: 
        fsType: ext3
        volumeID: # shared-storage-2
    ```
    The volumes ID are part of the output of the HEAT deployment described above.

    The `cinder-storage.yaml` specification file shown above simply states that two persistent volumes should be created.
    The first one should be named `pv-openstack-1` while the second one should be named `pv-openstack-2`.
    Both of them should be labelled with the `app: wordpress` label, they should have a capacity of `20GiB`, they should be writable by no more than 1 pod at a time, they should be mounted using the `ext3` file system, and they should be backed by the two volumes defined by the HEAT template desribed a few sections above.
    Finally, the two volumes share the `storageClassName` property value which refers to the `cinder-volume` storage class, which, in turn, is defined within the `cinder-storage.yaml` specification file too:
    ```yaml
    kind: StorageClass
    apiVersion: storage.k8s.io/v1beta1
    metadata:
      # to be used as value for annotation:
      # volume.beta.kubernetes.io/storage-class
      name: cinder-volume
    provisioner: kubernetes.io/cinder
    parameters:
      # openstack availability zone
      availability: nova
    ```
    Such a storage class simply states that the volumes are Cinder volumes, indeed, available within the `nova` zone.

    The `cinder-storage.yaml` file, along with all its resources, can be deployed by means of the following command:
    ```bash
    kubectl create -f cinder-storage.yaml
    ```

0. As the next step, the MySQL service along with its Persistent Volume Claims are deployed by means of the `mysql-deployment.yaml` specification file.
The file contains a resource of type `Deployment`. 
Deployments are essentially representations of replica set.
They usually specify that a given number of replicas of some particular Pod should be created.
For instance, the following Deployment

    ```yaml
    apiVersion: apps/v1 
    kind: Deployment
    metadata:
      # Name for the deployment
      name: wordpress-mysql 
      labels:
        # Label for the deploment
        app: wordpress
    spec:
      selector:
        # the deployment will include all pods labelled with the following key-value pairs
        matchLabels:
          app: wordpress
          tier: mysql
      strategy:
        # upon update, pods are simply deleted and re-created ex novo
        type: Recreate
      # the template for the pod(s) composing this deployment
      template:
        metadata:
          # labels for the pod
          labels:
            app: wordpress
            tier: mysql
        spec:
          # the pod will be composed of the following containers
          containers:
            # container 1, image: mysql:5.6, available on DockerHub
            - image: mysql:5.6
              name: mysql
              env:
                # environment variable containing the MySQL root password
                # the value is taken from the secret defined above
                - name: MYSQL_ROOT_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: mysql-pass
                      key: password
              # the ports to be exposed by the container
              ports:
                - containerPort: 3306 # MySQL default port
                  name: mysql
              volumeMounts:
                # reference to the `mysql-persistent-storage` defined below
                - name: mysql-persistent-storage
                  # the volume will be mounted on the container at the following position
                  mountPath: /var/lib/mysql
          volumes:
            # a volume to be used by the containers composing the deployment
            - name: mysql-persistent-storage
              # the volume will be dynamically selected by means of a Persistent Volume Clam named `mysql-pvc`
              persistentVolumeClaim:
                claimName: mysql-pvc
    ```
    
    This deployment will eventually need to reserve one of the two Persistent Volumes created before.
    To do so, the `mysql-deployment.yaml` specification file also contains a resource of type `PersistenVolumeClaim`:
    ```yaml
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: mysql-pvc
      labels:
        app: wordpress
    spec:
      storageClassName: cinder-volume
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: 20Gi
    ```
    The persistent volumene claim essentially represent a request for a volume of `20GiB` being part of the `cinder-volume` storage class.
    Both the two persistent volumes defined before match this claim, so any of them may be assigned to this deployment.

    Finally, a `Service` exposing the MySQL deployment to the other pods possibly executing on the cluster need to be defined.
    This is why the `mysql-deployment.yaml` specification file also include a resource of type `Service`:
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      # a name for the service
      name: wordpress-mysql
      labels:
        app: wordpress
    spec:
      # the service will be available on all machines composing the cluster by means of a 
      # dynamically selected, cluster-wise IP, on port 3306
      type: ClusterIP 
      clusterIP: None
      ports:
        - port: 3306
      selector:
        # selects all pods having the following labels
        app: wordpress
        tier: mysql
    ```
    Such a `Service` resource simply creates a virtual IP (i.e., the `ClusterIP`) which can be used within the cluster to interact with the MySQL database.
    Also notice that, by defining a service, a cluster-wise virtual hostname is implicity defined, equals to the service name, i.e. `wordpress-mysql`.
    This is how the MySQL database will be actually contacted by Wordpress instances. 

    The `mysql-deployment.yaml` file, along with all its resources, can be deployed by means of the following command:
    ```bash
    kubectl create -f mysql-deployment.yaml
    ```

<!-- 0. The Wordpress service can finally be deployed by means of the `wordpress-deployment.yaml` specification file: -->
0. As the next step, the Wordpress service is deployed by means of the `wordpress-deployment.yaml` specification file.
In particular it defines the following Deployment resource:
    ```yaml
    apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
    kind: Deployment
    metadata:
      # Name for the deployment
      name: wordpress
      labels:
        # Label for the deploment
        app: wordpress
    spec:
      # the amount of replicas composing the deployment
      replicas: 4
      selector:
        # the deployment will include all pods labelled with the following key-value pairs
        matchLabels:
          app: wordpress
          tier: frontend
      strategy:
         # upon update, pods are simply deleted and re-created ex novo
        type: Recreate
      # the template for the pod(s) composing this deployment
      template:
        metadata:
          # labels for the pod
          labels:
            app: wordpress
            tier: frontend
        spec:
          # each pod will be composed of the following containers
          containers:
            # container 1, image: wordpress:4.8-apache, available on DockerHub
            - image: wordpress:4.8-apache
              name: wordpress
              env:
                # enviroment variable containing the hostname where the MySQL database is supposed to be available
                - name: WORDPRESS_DB_HOST
                  value: wordpress-mysql
                # environment variable containing the MySQL password
                # the value is taken from the secret defined previously
                - name: WORDPRESS_DB_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: mysql-pass
                      key: password
              # the ports to be exposed by the containers
              ports:
                - containerPort: 80
                  name: wordpress

    ```
    representing the frontend tier of our Wordpress application. 

    Again, notice the reference to the previously defined secret.

    Also notice that the service is backed by 4 replicas. 
    This is possible because the Wordpress state is actually stored on the database, making the frotend tier replicable without consistency problems.

    In order to expose the wordpress service, we need to actually create a `Service` resource.
    Because of this need, the `mysql-deployment.yaml` file includes a resource definition of such a type:
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      # the name of the service
      name: wordpress
      # labels for the service
      labels:
        app: wordpress
    spec:
      # the service will be available on all machines composing the cluster by means of a 
      # dynamically selected, cluster-wise IP, on port 80, which will be subject to load balancing
      type: LoadBalancer
      ports:
        # The port on which the service should be available within the cluster
        - port: 80
      # All the pods being labelled with the following key=value pairs will be part of the service
      selector:
        app: wordpress
        tier: frontend
    ```
    
    The service is of type `LoadBalancer`.
    This is the type to be used for services assuming some external load-balancing to occur.
    To achieve such a load balancing, we will need the following and last step to deploy an _ad hoc_ resource of type `Ingress`.
    Notice that, according to [the Kubernetes Service documentation](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer) the type `LoadBalancer` is a supertype of type `ClusterIP` (described before), which implies that the service will be available on port 80 on all the machines composing the cluster.

    The `wordpress-deployment.yaml` file can be deployed by means of the following command:
    ```bash
    kubectl create -f wordpress-deployment.yaml
    ```

0. The last step is aimed at making the Wordpress service accessible from the Internet---provided that the `master-node` is assigned with a floating public IP. 
To do so, the `ingress.yaml` file must be deployed.
Before that, the file must be updated in order to include the `master-node`'s public IP, as shown below:
    ```yaml
    apiVersion: extensions/v1beta1
    kind: Ingress
    metadata:
      # A name for the Ingress resource
      name: ingress-over-traefik
      # Annotation telling ingress to use traefik implementation
      annotations:
        kubernetes.io/ingress.class: traefik
    spec:
      # Routing rules
      rules:
      # HTTP requests coming directed towards the <master-node IP>.xip.io URL... 
      - host: <master-node IP>.xip.io   # <-- IP here
        http:
          paths:
          # ... should be redirected towards the wordpress service
          - backend:
              serviceName: wordpress
              servicePort: 80
    ```
    This snippet defines a resource of type `Ingress`.
    According to the [Kubernetes documentation](https://kubernetes.io/docs/concepts/services-networking/ingress/), such a sort of resource can be employed to take care of load-balancing requests to one or more already deployed services.
    Here, in particular, we state the ingoing web requests should be redirected towards the `wordpress` service.

    The `ingress.yaml` file can be deployed by means of the following command:
    ```bash
    kubectl create -f ingress.yaml
    ```

    The last command is aimed at exposing the Wordpress service on the Internet on port 80, along with all the Wordpress replicas, which will serve the clients requests in a round-robin fashion, keeping their state on the common MySQL database.

    In particular, the Ingress resource will only accept HTTP requested directed towards the `<master-node IP>.xip.io` URL.
    The `.xip.io` suffix is required because the `spec.rules[i].host` field of an `Ingress` resource must be an host name and cannot be a raw IP.
    To circumvent such limitation, we rely on the [xip.io](http://xip.io) service which resolves any URL in the form `<whatever>.<someIP>.xip.io` as `<someIP>`.
    
    Finally, one can check the correctness of our solution by connecting to the `http://<master-node IP>.xip.io` URL with any web browser from within the CNAF VPN.
    If the Wordpress configuration Wizard is shown, then your setup succeded.

In any case, the Wordpress service should be now listening on the same port for all nodes composing the cluster.
This is due to the `LoadBalancer` type being a supertype of `ClusterIP` and `NodePort`.
By running the `kubectl get service` command, it is possible to reveal the port the Wordpress service is listening on.
It should provide an outcome similar to the following one:
```
NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
kubernetes                     ClusterIP      10.96.0.1        <none>        443/TCP          2h
wordpress                      LoadBalancer   10.102.62.254    <pending>     80:30307/TCP     1h
wordpress-mysql                ClusterIP      None             <none>        3306/TCP         1h
```
showing that the Wordpress service's `NodePort` is `30307`.

#### Experiments with Kubernetes

If the previous steps were successfull, there should exist `4` replicas of the Wordpress frontend.
One can reveal the amount of replicas by means of the following command:
```bash
kubectl get deploy
```
which should provide an outcome similar to the following one:
```
NAME                              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
wordpress                         4         4         4            0           1m
wordpress-mysql                   1         1         1            1           1m
```

Each replica is indeed a Kubernetes pod. 
In fact, the command:
```bash
kubectl get pods
```
should show that 5 pods in total are currently running:
```
NAME                                               READY   STATUS    RESTARTS   AGE
wordpress-78597bd4fc-6gntx                         1/1     Running   0          1m
wordpress-78597bd4fc-9lrvz                         1/1     Running   0          1m
wordpress-78597bd4fc-nrcxz                         1/1     Running   0          1m
wordpress-78597bd4fc-sx29v                         1/1     Running   0          1m
wordpress-mysql-d5df7656c-smvgs                    1/1     Running   0          1m
```
Notice that the 4 `wordpress-78597bd4fc-*` pods are indeed the replicas composing the `wordpress` deployment.

One may scale up/down the Wordpress service by dynamically changing the amount of replicas composing the `wordpress` deployment.
The following command pursues this purpose:
```bash
kubectl scale deployment wordpress --replicas=<amount of replicas>
```

For instance, if we run the command
```bash
kubectl scale deployment wordpress --replicas=10
```
and then we keep running the command
```bash
kubectl get deploy
```
we should initially see an output similar to the following one:
```
NAME                              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
wordpress                         10        10        10           4           3m
wordpress-mysql                   1         1         1            1           3m
```
which will eventually become:
```
NAME                              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
wordpress                         10        10        10           10          4m
wordpress-mysql                   1         1         1            1           4m
```

One may also try to kill one of the replicas to demonstrate how Kubernetes is able to restore the desired state.

To do so, we first need to discover the pod ID, by means of the:
```bash
kubectl get pods
```
which should list a number of pods, one for each replica of each deployment, along with their IDs:
```bash
NAME                                               READY   STATUS    RESTARTS   AGE
wordpress-78597bd4fc-6gntx                         1/1     Running   0          4m
wordpress-78597bd4fc-88zkc                         1/1     Running   0          1m
wordpress-78597bd4fc-9lrvz                         1/1     Running   0          4m
wordpress-78597bd4fc-dr4x7                         1/1     Running   0          1m
wordpress-78597bd4fc-kt2wb                         1/1     Running   0          1m
wordpress-78597bd4fc-nrcxz                         1/1     Running   0          4m
wordpress-78597bd4fc-r6rlc                         1/1     Running   0          1m
wordpress-78597bd4fc-rdww4                         1/1     Running   0          1m
wordpress-78597bd4fc-rwkcz                         1/1     Running   0          1m
wordpress-78597bd4fc-sx29v                         1/1     Running   0          4m
wordpress-mysql-d5df7656c-smvgs                    1/1     Running   0          5m
```

We must now select a pod to be killed, say `wordpress-78597bd4fc-6gntx`, and use its ID within the following command:
```bash
kubectl delete pod wordpress-78597bd4fc-6gntx --force
```

By running again the `kubectl get deploy` command several times, we should notice the current amount of replicas is initially decreased, even if it will eventually increase, proving that Kubernetes is able to automatically restore the desired service configuration.

The correct way to scale out the Wordpress service is to run the following command:
```bash
kubectl scale deployment wordpress --replicas=3
```
which whould eventually bring the system towards the following configuration:
```
NAME                              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
wordpress                         3         3         3            3           7m
wordpress-mysql                   1         1         1            1           7m
```

#### The load balancing

In order to test the load balancing feature on Kubernetes too, we exploit the Wordpress logs.
In fact, each Wordpress replica produces a log file which is inspectable by means of the command:
```bash
kubectl logs <pod id>
```

The log file contains (at least) a line for each HTTP request received by the Wordpress replica.
Such lines contain the uppercase name of the HTTP methods specified by the HTTP requests.
It is therefore sufficient to count the log lines containing the `GET` string to compute a lower bound for the amount of requests received by a particular replica.
This can be easily achieved by means of the following command:
```bash
kubectl logs <pod id> | grep "GET" | wc -l
```
We can then retrieve the list of pods composing the `wordpress` deployment -- i.e. the pods labelled with the `tier=frontend` label -- by means of the following command:
```bash
kubectl get pod -l "tier=frontend" -o=jsonpath='{range .items[*]}{.metadata.name}{"\n"}'
```
Finally, we can combine the two commads above, into a function, say `count_lines_in_pods`, to be employed for printing the amount of requests served by each replica:
```bash
function count_lines_in_pods {
    for pod in $(kubectl get pod -l $1 -o=jsonpath='{range .items[*]}{.metadata.name}{"\n"}'); do
        echo "$pod : $(kubectl logs $pod | grep $2 | wc -l)";
    done
}
```
For instance, invoking the command `count_lines_in_pods "tier=frontend" "GET"`, should provide an output similar to the following one:
```
wordpress-78597bd4fc-9lrvz : 0
wordpress-78597bd4fc-nrcxz : 0
wordpress-78597bd4fc-sx29v : 0
```

We can ensure the load balancing is actually functioning by running the following loop:
```bash
for i in `seq 1 100`; do 
    curl localhost:30307 ; 
    count_lines_in_pods "tier=frontend" "GET"; 
    echo "---"; 
done
```
which should provide an output similar to the following one:
```
wordpress-78597bd4fc-9lrvz : 0
wordpress-78597bd4fc-nrcxz : 0
wordpress-78597bd4fc-sx29v : 1
---
wordpress-78597bd4fc-9lrvz : 0
wordpress-78597bd4fc-nrcxz : 1
wordpress-78597bd4fc-sx29v : 1
---
wordpress-78597bd4fc-9lrvz : 0
wordpress-78597bd4fc-nrcxz : 1
wordpress-78597bd4fc-sx29v : 2
---
wordpress-78597bd4fc-9lrvz : 1
wordpress-78597bd4fc-nrcxz : 1
wordpress-78597bd4fc-sx29v : 2
---
wordpress-78597bd4fc-9lrvz : 1
wordpress-78597bd4fc-nrcxz : 2
wordpress-78597bd4fc-sx29v : 2
---
wordpress-78597bd4fc-9lrvz : 1
wordpress-78597bd4fc-nrcxz : 2
wordpress-78597bd4fc-sx29v : 3
...
```
proving that load balancing actually occurs.

<!-- ---

Alternatively, we could again exploit the `gciatto/my-images:stateful-helloworld-server` image described before.

More procesely, we can define a `Deployment` resource containing several replicas of the `gciatto/my-images:stateful-helloworld-server` image, and expose it by means of a service, on port `8080`.

We then send several requests to the exposed service in order to reveal the Kubernetes internal `Deployment`-wise load-balancing.

To do so, we employ the `kube-stateful-helloworld-server.yaml` file, containing the following resource definitions:
```yaml
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  # A name for the Deployment resource
  name: statefull-helloworld-deployment
  # Labels for the deployment
  labels:
    app: statefull-helloworld
spec:
  # The amount of replicas composing the deployment
  replicas: 4
  # The deployment will include the pods having the following labels
  selector:
    matchLabels:
      app: statefull-helloworld
  strategy:
    # upon update, pods are simply deleted and re-created ex novo
    type: Recreate
  # the template for the pod(s) composing this deployment
  template:
    metadata:
      # labels for the pod
      labels:
        app: statefull-helloworld
    spec:
      # each pod will be composed of the following containers
      containers:
        - image: gciatto/my-images:stateful-helloworld-server
          name: statefull-helloworld-server
          # the ports to be exposed by the containers
          ports:
            - containerPort: 8080
              name: statefull-helloworld
```
representing the deployment wrapping the many replicas of the Stateful Hello-World server; and:
```yaml
apiVersion: v1
kind: Service
metadata:
  # a name for the service
  name: statefull-helloworld-service
  # label for the service
  labels:
    app: statefull-helloworld
spec:
  # the service will be available on all machines composing the cluster by means of a 
  # dynamically selected, cluster-wise IP, on port 8080, which will be subject to load balancing
  type: LoadBalancer
  ports:
    - port: 8080
  selector:
    # All the pods being labelled with the following key=value pairs will be part of the service
    app: statefull-helloworld
```
respresenting the serivce exposing the replicas of the Stateful Hello-World server to the whole cluster.

From within the cluster, we can now get the service's virtual IP, by means of the following command:
```
kubectl get service statefull-helloworld-service
```
which should provide the following output:
```
NAME                           TYPE        CLUSTER-IP     EXTERNAL-IP      PORT(S)    AGE
statefull-helloworld-service   ClusterIP   10.2.245.137   <pending>        8080/TCP   54s
```
notice that the service cluster-wise IP is `10.2.245.137`.

Finally, we can demonstrate that requests are routed to different replicas by running the following command
```bash
for i in `seq 1 100`; do curl 10.2.245.137:8888; echo ""; done
```
which will query the Stateful Hello-World server 100 times.
Similarly to the Docker Swarm case, a Round-Robin-like pattern should be evident by look at the HTTP responses provided as outout. -->

## Conclusion

We really enjoyed working with Docker and Kubernetes. 
We deeply understood what working with containers means: they really simplify and standardise the deployment process for (web) services and distributed application in general, making it mostly independent from the underlying infrastructure.
In fact, once the Docker Swarm or Kubernetes Cluster has been properly configured it becomes quite easy to deploy, scale, or update an application, ragerless of which and how many machines compose the cluster. 
This is great for reproducibility, which in turn is very important for research-oriented software products.
At the same time, of course, they both allow the user the allocation of services to machines by means of labels.

We also learned how to configure Docker and Kubernetes on a virtual cluster, mimicking a real wolrd scenario.
We consider it to be a valuable experience which allowed us to actually perceive the complexity both Kubernetes and Docker attempt to abstract away.

After practicing with both Kubernetes and Docker, we understood their differences and similarities with deeper detail.
Despite they are very similar -- at leat from the user point of view -- and they pursue analogous goals, we believe Docker (Swarm) to be more user friendly, both for what concerns its usage and its configuration, which often -- as in this case -- means lower flexibility.
In fact, Kubernetes allows for more degrees of freedom in deploying applications -- there including containers, pods, deployments, and services -- whereas Docker Swarm only supports a subset of them---namely, containers, services, and stacks.
Such differences are probably due to their maturity, which is greater in case of Kubernetes since it has been created [leveraging the experience of Borg](https://kubernetes.io/blog/2015/04/borg-predecessor-to-kubernetes/), a project maintained by Google for 15+ years.

Furthermore, we argue Kubernetes is the perfect deal for cloud platforms since it can become aware of any tenant possibly hosting it, for instance, in order to dinamically provision storage devices or to exploit load balancing facilities. Such features, are no avaible for Docker, to the best of my knowledge.

Concluding, we acknowledge that Kubernetes is more industry- and cloud-ready than Docker Swarm.