#!/bin/bash

# https://kubernetes.io/docs/setup/independent/install-kubeadm/
USERNAME=ubuntu
USER_HOME=/home/$USERNAME
HOSTNAME=`hostname`

function log {
    echo "    [GCis] $1"
}

echo ; echo "################### Giovanni Ciatto's initialisation script ####################"

echo "127.0.0.1		$HOSTNAME" >> /etc/hosts

apt-get -qq update
apt-get -qq  install -y docker.io=17.03.2-0ubuntu2~16.04.1 avahi-daemon avahi-discover avahi-utils libnss-mdns mdns-scan ipvsadm \
    && log "Docker & mDNS correctly installed" \
    || log "Problem while installing Docker or mDNS"

apt-get -qq update && apt-get -qq install -y apt-transport-https curl w3m \
    && log "HTTP utils correctly installed" \
    || log "Problem while installing HTTP utils"

modprobe ip_vs && log "IPVS modules loaded" || log "Problem while loading IPVS module"

service docker start && log "Docker service started" || log "Problem while starting the docker service"

usermod -a -G docker $USERNAME && log "$USERNAME added to the docker group" || log "Problem while adding $USERNAME to the docker group"

echo "################ End of Giovanni Ciatto's initialisation script ################" ; echo